# Public Taxon Query
#
# Query taxons and concept relations between them
#
# OpenAPI spec version: 1
# Contact: TODO-email@bfn.de
# Generated by: https://github.com/swagger-api/swagger-codegen.git


#' TaxonRelationsResult Class
#'
#' @field relations 
#' @field hierarchies 
#' @field taxons 
#'
#' @importFrom R6 R6Class
#' @importFrom jsonlite fromJSON toJSON
#' @export
TaxonRelationsResult <- R6::R6Class(
  'TaxonRelationsResult',
  public = list(
    `relations` = NULL,
    `hierarchies` = NULL,
    `taxons` = NULL,
    initialize = function(`relations`, `hierarchies`, `taxons`){
      if (!missing(`relations`)) {
        stopifnot(is.list(`relations`), length(`relations`) != 0)
        lapply(`relations`, function(x) stopifnot(R6::is.R6(x)))
        self$`relations` <- `relations`
      }
      if (!missing(`hierarchies`)) {
        stopifnot(R6::is.R6(`hierarchies`))
        self$`hierarchies` <- `hierarchies`
      }
      if (!missing(`taxons`)) {
        stopifnot(R6::is.R6(`taxons`))
        self$`taxons` <- `taxons`
      }
    },
    toJSON = function() {
      TaxonRelationsResultObject <- list()
      if (!is.null(self$`relations`)) {
        TaxonRelationsResultObject[['relations']] <- lapply(self$`relations`, function(x) x$toJSON())
      }
      if (!is.null(self$`hierarchies`)) {
        TaxonRelationsResultObject[['hierarchies']] <- self$`hierarchies`$toJSON()
      }
      if (!is.null(self$`taxons`)) {
        TaxonRelationsResultObject[['taxons']] <- self$`taxons`$toJSON()
      }

      TaxonRelationsResultObject
    },
    fromJSON = function(TaxonRelationsResultJson) {
      TaxonRelationsResultObject <- jsonlite::fromJSON(TaxonRelationsResultJson)
      if (!is.null(TaxonRelationsResultObject$`relations`)) {
        self$`relations` <- lapply(TaxonRelationsResultObject$`relations`, function(x) {
          relationsObject <- TaxonRelation$new()
          relationsObject$fromJSON(jsonlite::toJSON(x, auto_unbox = TRUE))
          relationsObject
        })
      }
      if (!is.null(TaxonRelationsResultObject$`hierarchies`)) {
        hierarchiesObject <- TaxonRelationsResultHierarchies$new()
        hierarchiesObject$fromJSON(jsonlite::toJSON(TaxonRelationsResultObject$hierarchies, auto_unbox = TRUE))
        self$`hierarchies` <- hierarchiesObject
      }
      if (!is.null(TaxonRelationsResultObject$`taxons`)) {
        taxonsObject <- TaxonRelationsResultTaxons$new()
        taxonsObject$fromJSON(jsonlite::toJSON(TaxonRelationsResultObject$taxons, auto_unbox = TRUE))
        self$`taxons` <- taxonsObject
      }
    },
    toJSONString = function() {
       sprintf(
        '{
           "relations": [%s],
           "hierarchies": %s,
           "taxons": %s
        }',
        lapply(self$`relations`, function(x) paste(x$toJSON(), sep=",")),
        self$`hierarchies`$toJSON(),
        self$`taxons`$toJSON()
      )
    },
    fromJSONString = function(TaxonRelationsResultJson) {
      TaxonRelationsResultObject <- jsonlite::fromJSON(TaxonRelationsResultJson)
      self$`relations` <- lapply(TaxonRelationsResultObject$`relations`, function(x) TaxonRelation$new()$fromJSON(jsonlite::toJSON(x, auto_unbox = TRUE)))
      TaxonRelationsResultHierarchiesObject <- TaxonRelationsResultHierarchies$new()
      self$`hierarchies` <- TaxonRelationsResultHierarchiesObject$fromJSON(jsonlite::toJSON(TaxonRelationsResultObject$hierarchies, auto_unbox = TRUE))
      TaxonRelationsResultTaxonsObject <- TaxonRelationsResultTaxons$new()
      self$`taxons` <- TaxonRelationsResultTaxonsObject$fromJSON(jsonlite::toJSON(TaxonRelationsResultObject$taxons, auto_unbox = TRUE))
    }
  )
)
