# Public Taxon Query
#
# Query taxons and concept relations between them
#
# OpenAPI spec version: 1
# Contact: TODO-email@bfn.de
# Generated by: https://github.com/swagger-api/swagger-codegen.git


#' Checklist Class
#'
#' @field kingdom 
#' @field cycle 
#' @field speciesgroup-id 
#' @field region 
#' @field citation 
#' @field label 
#' @field id 
#' @field speciesgroup 
#' @field uri 
#' @field version 
#'
#' @importFrom R6 R6Class
#' @importFrom jsonlite fromJSON toJSON
#' @export
Checklist <- R6::R6Class(
  'Checklist',
  public = list(
    `kingdom` = NULL,
    `cycle` = NULL,
    `speciesgroup-id` = NULL,
    `region` = NULL,
    `citation` = NULL,
    `label` = NULL,
    `id` = NULL,
    `speciesgroup` = NULL,
    `uri` = NULL,
    `version` = NULL,
    initialize = function(`kingdom`, `cycle`, `speciesgroup-id`, `region`, `citation`, `label`, `id`, `speciesgroup`, `uri`, `version`){
      if (!missing(`kingdom`)) {
        stopifnot(is.character(`kingdom`), length(`kingdom`) == 1)
        self$`kingdom` <- `kingdom`
      }
      if (!missing(`cycle`)) {
        stopifnot(is.character(`cycle`), length(`cycle`) == 1)
        self$`cycle` <- `cycle`
      }
      if (!missing(`speciesgroup-id`)) {
        stopifnot(is.numeric(`speciesgroup-id`), length(`speciesgroup-id`) == 1)
        self$`speciesgroup-id` <- `speciesgroup-id`
      }
      if (!missing(`region`)) {
        stopifnot(is.character(`region`), length(`region`) == 1)
        self$`region` <- `region`
      }
      if (!missing(`citation`)) {
        stopifnot(is.character(`citation`), length(`citation`) == 1)
        self$`citation` <- `citation`
      }
      if (!missing(`label`)) {
        stopifnot(is.character(`label`), length(`label`) == 1)
        self$`label` <- `label`
      }
      if (!missing(`id`)) {
        stopifnot(is.numeric(`id`), length(`id`) == 1)
        self$`id` <- `id`
      }
      if (!missing(`speciesgroup`)) {
        stopifnot(is.character(`speciesgroup`), length(`speciesgroup`) == 1)
        self$`speciesgroup` <- `speciesgroup`
      }
      if (!missing(`uri`)) {
        stopifnot(is.character(`uri`), length(`uri`) == 1)
        self$`uri` <- `uri`
      }
      if (!missing(`version`)) {
        stopifnot(is.numeric(`version`), length(`version`) == 1)
        self$`version` <- `version`
      }
    },
    toJSON = function() {
      ChecklistObject <- list()
      if (!is.null(self$`kingdom`)) {
        ChecklistObject[['kingdom']] <- self$`kingdom`
      }
      if (!is.null(self$`cycle`)) {
        ChecklistObject[['cycle']] <- self$`cycle`
      }
      if (!is.null(self$`speciesgroup-id`)) {
        ChecklistObject[['speciesgroup-id']] <- self$`speciesgroup-id`
      }
      if (!is.null(self$`region`)) {
        ChecklistObject[['region']] <- self$`region`
      }
      if (!is.null(self$`citation`)) {
        ChecklistObject[['citation']] <- self$`citation`
      }
      if (!is.null(self$`label`)) {
        ChecklistObject[['label']] <- self$`label`
      }
      if (!is.null(self$`id`)) {
        ChecklistObject[['id']] <- self$`id`
      }
      if (!is.null(self$`speciesgroup`)) {
        ChecklistObject[['speciesgroup']] <- self$`speciesgroup`
      }
      if (!is.null(self$`uri`)) {
        ChecklistObject[['uri']] <- self$`uri`
      }
      if (!is.null(self$`version`)) {
        ChecklistObject[['version']] <- self$`version`
      }

      ChecklistObject
    },
    fromJSON = function(ChecklistJson) {
      ChecklistObject <- jsonlite::fromJSON(ChecklistJson)
      if (!is.null(ChecklistObject$`kingdom`)) {
        self$`kingdom` <- ChecklistObject$`kingdom`
      }
      if (!is.null(ChecklistObject$`cycle`)) {
        self$`cycle` <- ChecklistObject$`cycle`
      }
      if (!is.null(ChecklistObject$`speciesgroup-id`)) {
        self$`speciesgroup-id` <- ChecklistObject$`speciesgroup-id`
      }
      if (!is.null(ChecklistObject$`region`)) {
        self$`region` <- ChecklistObject$`region`
      }
      if (!is.null(ChecklistObject$`citation`)) {
        self$`citation` <- ChecklistObject$`citation`
      }
      if (!is.null(ChecklistObject$`label`)) {
        self$`label` <- ChecklistObject$`label`
      }
      if (!is.null(ChecklistObject$`id`)) {
        self$`id` <- ChecklistObject$`id`
      }
      if (!is.null(ChecklistObject$`speciesgroup`)) {
        self$`speciesgroup` <- ChecklistObject$`speciesgroup`
      }
      if (!is.null(ChecklistObject$`uri`)) {
        self$`uri` <- ChecklistObject$`uri`
      }
      if (!is.null(ChecklistObject$`version`)) {
        self$`version` <- ChecklistObject$`version`
      }
    },
    toJSONString = function() {
       sprintf(
        '{
           "kingdom": %s,
           "cycle": %s,
           "speciesgroup-id": %d,
           "region": %s,
           "citation": %s,
           "label": %s,
           "id": %d,
           "speciesgroup": %s,
           "uri": %s,
           "version": %d
        }',
        self$`kingdom`,
        self$`cycle`,
        self$`speciesgroup-id`,
        self$`region`,
        self$`citation`,
        self$`label`,
        self$`id`,
        self$`speciesgroup`,
        self$`uri`,
        self$`version`
      )
    },
    fromJSONString = function(ChecklistJson) {
      ChecklistObject <- jsonlite::fromJSON(ChecklistJson)
      self$`kingdom` <- ChecklistObject$`kingdom`
      self$`cycle` <- ChecklistObject$`cycle`
      self$`speciesgroup-id` <- ChecklistObject$`speciesgroup-id`
      self$`region` <- ChecklistObject$`region`
      self$`citation` <- ChecklistObject$`citation`
      self$`label` <- ChecklistObject$`label`
      self$`id` <- ChecklistObject$`id`
      self$`speciesgroup` <- ChecklistObject$`speciesgroup`
      self$`uri` <- ChecklistObject$`uri`
      self$`version` <- ChecklistObject$`version`
    }
  )
)
